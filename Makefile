.PHONY: nvim nvim-gtk

# Pull

nvim-pull:
	cp ~/.config/nvim/ginit.vim nvim
	cp ~/.config/nvim/init.vim nvim

nvim-gtk-pull:
	cp ~/.config/nvim-gtk/plugs.toml nvim-gtk
	cp ~/.config/nvim-gtk/settings.vim nvim-gtk
	cp ~/.config/nvim-gtk/window.toml nvim-gtk


# Install

nvim-install:
	mkdir -p ~/.config/nvim
	cp nvim/* ~/.config/nvim

nvim-gtk-install:
	mkdir -p ~/.config/nvim-gtk
	cp nvim-gtk/* ~/.config/nvim-gtk
	@echo
	@echo
	@echo "###############################################"
	@echo "#                                             #"
	@echo "#        Install neovim-plug from AUR         #"
	@echo "#                                             #"
	@echo "#   After opening nvim-gtk use :PlugInstall   #"
	@echo "#                                             #"
	@echo "###############################################"
