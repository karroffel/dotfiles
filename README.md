# Configuration files

The Makefile contains two categories of commands: "pull" and "install".

Pull commands update the files in this repository with the ones currently installed on the system.

Install commands install files from this repository on the system.


Available `make` commands:

 - `nvim-pull`
 - `nvim-gtk-pull`

 - `nvim-install`
 - `nvim-gtk-install`
