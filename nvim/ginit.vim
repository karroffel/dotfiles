if exists('g:GtkGuiLoaded')
    set background=light
    colorscheme PaperColor
    call rpcnotify(1, 'Gui', 'Font', 'Fira Code Light 11')
    call rpcnotify(1, 'Gui', 'Option', 'Cmdline', 1)
endif
