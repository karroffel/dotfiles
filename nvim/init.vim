"
" General stuff
"

set colorcolumn=80
set number
set relativenumber


"
" Rust stuff
"

autocmd BufWrite *.rs :RustFmt

"
" Why3 stuff
"

" Make sure the syntax file is loadable
set runtimepath^=/usr/share/why3/vim

" somehow ftdetect doesn't work
autocmd BufRead,BufNewFile *.why,*mlw set filetype=why3

" Syntax doesn't use 4 spaces, override that
autocmd BufRead,BufNewFile *.why,*mlw set expandtab
autocmd BufRead,BufNewFile *.why,*mlw set shiftwidth=4

