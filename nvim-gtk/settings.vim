call plug#begin()
Plug 'scrooloose/syntastic', { 'as': 'Syntastic' }
Plug 'rust-lang/rust.vim', { 'as': 'rust.vim' }
Plug 'nlknguyen/papercolor-theme', { 'as': 'papercolor-theme' }
call plug#end()
